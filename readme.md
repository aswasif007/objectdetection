# ObjectDetection

This is a image processing project.
The primary purpose is to detect objects in running video feed and recognise them.

### How to use:
* run **python main.py <image-frame address>**
* Output will be stored in the directory.
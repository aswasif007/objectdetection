import numpy as np
import cv2
import os
import sys


def centerOfGravity(pts):							# Finds center of gravity of the match points.
	x,y=float(pts[0][0][0]),float(pts[0][0][1])

	for i in range(1,len(pts)):
		x1,y1=float(pts[i][0][0]),float(pts[i][0][1])
		x1-=x
		y1-=y
		x+=x1/(i+1)
		y+=y1/(i+1)

	return int(x),int(y)
#-----------------


def drawMatches(img1,img2,src,dst):					# Debug function. Draws mapping.
	h1,w1 = img1.shape[:2]
	h2,w2 = img2.shape[:2]
	vis = np.zeros((max(h1, h2), w1+w2), np.uint8)
	vis[:h1, :w1] = img1
	vis[:h2, w1:w1+w2] = img2

	vis=cv2.cvtColor(vis, cv2.COLOR_GRAY2RGB)

	for i in range(0,len(src)):
		pt1=(src[i][0][0],src[i][0][1])
		pt2=(w1+dst[i][0][0],dst[i][0][1])

		cv2.circle(vis,pt1, 6, (0,255,0), 1)
		cv2.circle(vis,pt2, 6, (0,255,0), 1)
		cv2.line(vis,pt1,pt2,(0,255,0),1)

	return vis
#----------------


def shapeCoeff(src,dst):
	src_d=float(src[0][0][0]-src[1][0][0])**2 + (src[0][0][1]-src[1][0][1])**2
	dst_d=float(dst[0][0][0]-dst[1][0][0])**2 + (dst[0][0][1]-dst[1][0][1])**2

	return (dst_d/src_d)**0.5
#----------------


def extractSampleData():			
	samples="Samples"			# Directory-name

	surf=cv2.SURF(400)
	specs=os.listdir(samples)

	data=[]

	for spec in specs:
		files=os.listdir(samples+'/'+spec)

		temp=[spec]

		for f in files:
			f=samples+'/'+spec+'/'+f
			img=cv2.imread(f)
			img=cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
			res=surf.detectAndCompute(img,None)
			temp.append(res)

		data.append(temp)

	return data
#-------------


def getFlannBasedMatcher():
	FLANN_INDEX_KDTREE=0
	index_params=dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
	search_params=dict(checks=50)

	flann=cv2.FlannBasedMatcher(index_params, search_params)

	return flann
#--------------

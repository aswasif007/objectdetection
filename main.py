import cv2
from matplotlib import pyplot as plt
from rabbit_lib import *

if(len(sys.argv)!=2):
	print 'Usage:'
	print 'main.py <image path>'
	exit(0)


# Globals
sample_data=extractSampleData()			# Extracts the data from sample images
flann=getFlannBasedMatcher()
surf=cv2.SURF(400)
#----------


def process(frame):
	gray=cv2.cvtColor(frame,cv2.COLOR_RGB2GRAY)
	rkp,rdes=surf.detectAndCompute(gray,None)			# Calculates keypoints and descriptors

	for item in sample_data:							# Checking for each object in sample dataset
		name=item[0]
		
		point_count=0
		match_count=0
		max_match=0
		mpt=[]

		for kp,des in item[1:]:							# Calculate match for each sample for each object,
			matches=flann.knnMatch(des,rdes,k=2)		# since an object can have multiple samples

			good=[]
			for m,n in matches:							# Filtering the matches.
				if m.distance < 0.7*n.distance:
					good.append(m)

			match_count+=len(good)
			point_count+=len(kp)
			match=float(len(good))/len(kp)*100.0		# Calculate match percentage for one sample
			max_match=max(max_match,match)

			tmp=np.int32([ rkp[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
			for i in tmp:
				mpt.append(i)

		total_match=float(match_count)/point_count*100.0	# Calculate total match percentage for an object

		if(total_match>3.0 or max_match>5.0 or match_count>25):		# Thresholding.
			print name+' found.'
			cx,cy=centerOfGravity(mpt)
			cv2.rectangle(frame,(cx-100,cy-100),(cx+100,cy+100), (0,255,0), 4)
			cv2.putText(frame,name,(cx-100,cy-100-20), cv2.FONT_HERSHEY_SIMPLEX, 1,(0,255,0),2)


frame=cv2.imread(sys.argv[1])
process(frame)

cv2.imwrite('result.jpg',frame)

# cv2.imshow('test',frame)
# cv2.waitKey(0)